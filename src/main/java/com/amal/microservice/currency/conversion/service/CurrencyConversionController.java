package com.amal.microservice.currency.conversion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;


@RestController
public class CurrencyConversionController {

    @Autowired
    private Environment environment;

    @Autowired
    private CurrencyExchangeProxy proxy;

    //This method works using old way of calling REST API using RestTemplate
    @GetMapping("/currency-conversion/from/{from}/to/{to}/quantity/{quantity}")
    public CurrencyConversion getCurrencyConverted(@PathVariable String from, @PathVariable String to, @PathVariable Double quantity) {

        //Map uri variable to field names
        HashMap<String, String> uriVariableMap = new HashMap<>();
        uriVariableMap.put("from", from);
        uriVariableMap.put("to", to);
        //Make a REST API call
        ResponseEntity<CurrencyConversion> currencyConversion = new RestTemplate().getForEntity("http://localhost:8000/currency-exchange/from/{from}/to/{to}", CurrencyConversion.class,
                uriVariableMap);
        CurrencyConversion conversion = currencyConversion.getBody();
        return new CurrencyConversion(10001L, from, to, conversion.getMultiple()
                , quantity, conversion.getMultiple().multiply(BigDecimal.valueOf(quantity)).doubleValue(), conversion.getEnvironment());
    }

    //This method works using Feign proxy
    @GetMapping("/currency-conversion-feign/from/{from}/to/{to}/quantity/{quantity}")
    public CurrencyConversion getCurrencyConvertedFeign(@PathVariable String from, @PathVariable String to, @PathVariable Double quantity) {
        CurrencyConversion currencyConversion = proxy.getExchangeValue(from, to);
        return new CurrencyConversion(10001L, from, to, currencyConversion.getMultiple()
                , quantity, currencyConversion.getMultiple().multiply(BigDecimal.valueOf(quantity)).doubleValue(), currencyConversion.getEnvironment()+", Feign");
    }
}
